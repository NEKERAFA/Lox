local lox = require "lox"

if #arg > 1 then
    print("Usage: llox.lua [script]")
    os.exit(64)
elseif #arg == 1 then
    lox.runfile(arg[1])
else
    lox.runprompt()
end