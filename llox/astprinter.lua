local utils = require "utils"

local astprinter = {}

function astprinter.new()
    return setmetatable({}, {
        __index = utils.__index(astprinter)
    })
end

function astprinter:print(statements)
    for _, statement in ipairs(statements) do
        print(self:visitstmt(statement))
    end
end

function astprinter:visitstmt(statement)
    return statement:accept(self)
end

function astprinter:visitprintstmt(statement)
    return self:parentezised("print", statement.innerexpr)
end

function astprinter:visitvarstmt(statement)
    return self:parentezised(statement.name.lexeme, statement.initializer)
end

function astprinter:visitexpressionstmt(statement)
    return statement.innerexpr:accept(self)
end

function astprinter:visitblockstmt(statement)
    local buffer = { "(", "block" }
    for _, value in ipairs(statement.statements) do
        table.insert(buffer, " ")
        table.insert(buffer, value:accept(self))
    end
    table.insert(buffer, ")")

    return table.concat(buffer, "")
end

function astprinter:visitbinaryexpr(expression)
    return self:parentezised(expression.operator.type, expression.left, expression.right)
end

function astprinter:visitgroupingexpr(expression)
    return self:parentezised("group", expression.expression)
end

function astprinter:visitliteralexpr(expression)
    return tostring(expression.value)
end

function astprinter:visitunaryexpr(expression)
    return self:parentezised(expression.operator.type, expression.right)
end

function astprinter:visitvariableexpr(expression)
    return "(" .. tostring(expression.name) .. ")"
end

function astprinter:visitassignexpr(expression)
    return self:parentezised(expression.name.lexeme, expression.value)
end

function astprinter:parentezised(name, ...)
    local buffer = { "(", name }
    for _, value in ipairs({...}) do
        table.insert(buffer, " ")
        table.insert(buffer, value:accept(self))
    end
    table.insert(buffer, ")")

    return table.concat(buffer, "")
end

--[[
local expression = expr.binary.new(
    expr.unary.new(
        token.new(tokentypes.MINUS, '-', nil, 1),
        expr.literal.new(123)
    ),
    token.new(tokentypes.STAR, '*', nil, 1),
    expr.grouping.new(
        expr.literal.new(45.67)
    )
)

print(astprinter.new():print(expression))
]]

return astprinter
