local function split(str, sep, plain)
    local ret = {}
    local last_pos = 1
    local start_pos, end_pos = str:find(sep, 1, plain or true)
    while start_pos do
        table.insert(ret, str:sub(last_pos, start_pos - 1))
        last_pos = end_pos + 1
        start_pos, end_pos = str:find(sep, last_pos, plain or true)
    end

    if last_pos < #str then
        table.insert(ret, str:sub(last_pos))
    end

    return ret
end

local function trim(str)
    return str:match("^%s*(.-)%s*$")
end

local function definetype(file, basename, name, fields)
    file:write(("-- We define the %s class\n"):format(name))
    file:write(("local %s = {}\n\n"):format(name))

    -- Constructor
    file:write(("function %s.new(%s)\n"):format(name, fields))
    file:write(("    return setmetatable({\n"):format(name))

    -- Store parameters in fields
    file:write("        ")
    for pos, field in ipairs(split(fields, ", ")) do
        if pos > 1 then file:write(" ") end
        file:write(("%s = %s,"):format(field, field))
    end
    file:write((" type = %q\n"):format(name))

    file:write("    }, {\n")
    file:write(("        __index = utils.__index(%s),\n"):format(name))
    file:write("        __tostring = utils.__tostring\n")
    file:write("    })\n")
    file:write("end\n\n")

    -- Visitor pattern
    file:write(("function %s:accept(visitor)\n"):format(name))
    file:write(("    return visitor:visit%s%s(self)\n"):format(name, basename))
    file:write("end\n\n")
end

local function defineast(output, basename, types)
    local path = output .. "/" .. basename .. ".lua"
    local fd = assert(io.open(path, "w+"))

    fd:write("local utils = require \"utils\"\n\n")

    -- the ast files
    for _, value in ipairs(types) do
        local name = trim(split(value, ":")[1])
        local fields = trim(split(value, ":")[2])
        definetype(fd, basename, name, fields)
    end

    fd:write("return { ")
    for pos, value in ipairs(types) do
        local name = trim(split(value, ":")[1])
        if pos > 1 then fd:write(", ") end
        fd:write(("%s = %s"):format(name, name))
    end
    fd:write(" }\n")

    fd:close()
end

if #arg ~= 1 then
    print("Usage: generate_ast <output dir>")
    os.exit(64)
end

local output_dir = arg[1]
defineast(output_dir, "expr", {
    "assign   : name, value",
    "binary   : left, operator, right",
    "grouping : expression",
    "literal  : value",
    "unary    : operator, right",
    "variable : name"
})

defineast(output_dir, "stmt", {
    "block      : statements",
    "expression : innerexpr",
    "print      : innerexpr",
    "var        : name, initializer",
})
