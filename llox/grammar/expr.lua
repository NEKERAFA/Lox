local utils = require "utils"

-- We define the assign class
local assign = {}

function assign.new(name, value)
    return setmetatable({
        name = name, value = value, type = "assign"
    }, {
        __index = utils.__index(assign),
        __tostring = utils.__tostring
    })
end

function assign:accept(visitor)
    return visitor:visitassignexpr(self)
end

-- We define the binary class
local binary = {}

function binary.new(left, operator, right)
    return setmetatable({
        left = left, operator = operator, right = right, type = "binary"
    }, {
        __index = utils.__index(binary),
        __tostring = utils.__tostring
    })
end

function binary:accept(visitor)
    return visitor:visitbinaryexpr(self)
end

-- We define the grouping class
local grouping = {}

function grouping.new(expression)
    return setmetatable({
        expression = expression, type = "grouping"
    }, {
        __index = utils.__index(grouping),
        __tostring = utils.__tostring
    })
end

function grouping:accept(visitor)
    return visitor:visitgroupingexpr(self)
end

-- We define the literal class
local literal = {}

function literal.new(value)
    return setmetatable({
        value = value, type = "literal"
    }, {
        __index = utils.__index(literal),
        __tostring = utils.__tostring
    })
end

function literal:accept(visitor)
    return visitor:visitliteralexpr(self)
end

-- We define the unary class
local unary = {}

function unary.new(operator, right)
    return setmetatable({
        operator = operator, right = right, type = "unary"
    }, {
        __index = utils.__index(unary),
        __tostring = utils.__tostring
    })
end

function unary:accept(visitor)
    return visitor:visitunaryexpr(self)
end

-- We define the variable class
local variable = {}

function variable.new(name)
    return setmetatable({
        name = name, type = "variable"
    }, {
        __index = utils.__index(variable),
        __tostring = utils.__tostring
    })
end

function variable:accept(visitor)
    return visitor:visitvariableexpr(self)
end

return { assign = assign, binary = binary, grouping = grouping, literal = literal, unary = unary, variable = variable }
