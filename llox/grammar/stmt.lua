local utils = require "utils"

-- We define the block class
local block = {}

function block.new(statements)
    return setmetatable({
        statements = statements, type = "block"
    }, {
        __index = utils.__index(block),
        __tostring = utils.__tostring
    })
end

function block:accept(visitor)
    return visitor:visitblockstmt(self)
end

-- We define the expression class
local expression = {}

function expression.new(innerexpr)
    return setmetatable({
        innerexpr = innerexpr, type = "expression"
    }, {
        __index = utils.__index(expression),
        __tostring = utils.__tostring
    })
end

function expression:accept(visitor)
    return visitor:visitexpressionstmt(self)
end

-- We define the print class
local print = {}

function print.new(innerexpr)
    return setmetatable({
        innerexpr = innerexpr, type = "print"
    }, {
        __index = utils.__index(print),
        __tostring = utils.__tostring
    })
end

function print:accept(visitor)
    return visitor:visitprintstmt(self)
end

-- We define the var class
local var = {}

function var.new(name, initializer)
    return setmetatable({
        name = name, initializer = initializer, type = "var"
    }, {
        __index = utils.__index(var),
        __tostring = utils.__tostring
    })
end

function var:accept(visitor)
    return visitor:visitvarstmt(self)
end

return { block = block, expression = expression, print = print, var = var }
