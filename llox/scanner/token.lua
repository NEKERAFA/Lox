local token = {}

local function __tostring(self)
    return string.format("type: %q, lexeme: %q, value: %s", self.type, self.lexeme, tostring(self.literal))
end

function token.new(type, lexeme, literal, line)
    return setmetatable({
        type = type,
        lexeme = lexeme,
        literal = literal,
        line = line
    }, {
        __tostring = __tostring
    })
end

return token
