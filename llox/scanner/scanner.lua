local token = require "scanner.token"
local tokentypes = require "scanner.tokentypes"
local utils = require "utils"

local scanner = {}

local keywords = {
    ["and"]    = tokentypes.AND,
    ["class"]  = tokentypes.CLASS,
    ["else"]   = tokentypes.ELSE,
    ["false"]  = tokentypes.FALSE,
    ["for"]    = tokentypes.FOR,
    ["fun"]    = tokentypes.FUN,
    ["if"]     = tokentypes.IF,
    ["nil"]    = tokentypes.NIL,
    ["or"]     = tokentypes.OR,
    ["print"]  = tokentypes.PRINT,
    ["return"] = tokentypes.RETURN,
    ["super"]  = tokentypes.SUPER,
    ["this"]   = tokentypes.THIS,
    ["true"]   = tokentypes.TRUE,
    ["var"]    = tokentypes.VAR,
    ["while"]  = tokentypes.WHILE,
}

-- Creates a new scanner
function scanner.new(source)
    return setmetatable({
        source = source,
        tokens = {},
        start = 1,
        current = 1,
        line = 1,
    }, {
        __index = utils.__index(scanner)
    })
end

-- Scan the tokens in the source string
function scanner:scantokens()
    while not self:isatend() do
        -- We are at the beginning of the next lexeme.
        self.start = self.current
        self.position = self.start
        self:scantoken()
    end

    table.insert(self.tokens, token.new(tokentypes.EOF, "", nil, self.line))
    return self.tokens
end

-- Scan a single token
function scanner:scantoken()
    local char = self:advance()

    -- Single characters
    if char == '(' then
        self:addtoken(tokentypes.LEFT_PAREN)
    elseif char == ')' then
        self:addtoken(tokentypes.RIGHT_PAREN)
    elseif char == '{' then
        self:addtoken(tokentypes.LEFT_BRACE)
    elseif char == '}' then
        self:addtoken(tokentypes.RIGHT_BRACE)
    elseif char == ',' then
        self:addtoken(tokentypes.COMMA)
    elseif char == '.' then
        self:addtoken(tokentypes.DOT)
    elseif char == '-' then
        self:addtoken(tokentypes.MINUS)
    elseif char == '+' then
        self:addtoken(tokentypes.PLUS)
    elseif char == ';' then
        self:addtoken(tokentypes.SEMICOLON)
    elseif char == '*' then
        self:addtoken(tokentypes.STAR)
    -- Operators
    elseif char == '!' then
        self:addtoken(self:match('=') and tokentypes.BANG_EQUAL or tokentypes.BANG)
    elseif char == '=' then
        self:addtoken(self:match('=') and tokentypes.EQUAL_EQUAL or tokentypes.EQUAL)
    elseif char == '<' then
        self:addtoken(self:match('=') and tokentypes.LESS_EQUAL or tokentypes.LESS)
    elseif char == '>' then
        self:addtoken(self:match('=') and tokentypes.GREATER_EQUAL or tokentypes.GREATER)
    -- Long lexemes
    elseif char == '/' then
        if self:match('/') then
            while self:peek() ~= '\n' and not self:isatend() do self:advance() end
        else
            self:addtoken(tokentypes.SLASH)
        end

    elseif char == ' ' or char == '\r' or char == '\t' then
    -- Ignore whitespace.

    elseif char == '\n' then
        self.line = self.line + 1
        self.position = 1

    -- Get string literals
    elseif char == '"' then self:string()

    -- Get decimal or integer numbers
    elseif scanner.isdigit(char) then self:number()

    -- Get identifiers (also reserved words)
    elseif scanner.isalpha(char) then self:identifier()

    else
        utils.error(self.line, "Unexpected character.")
    end
end

-- Consume the maximal munch of a identifier
function scanner:identifier()
    while scanner.isalphanumeric(self:peek()) do self:advance() end

    local text = self.source:sub(self.start, self.current - 1)
    local type = keywords[text]
    if type == nil then type = tokentypes.IDENTIFIER end
    self:addtoken(type)
end

-- Consume as many many digits as we find for the integer part of the literal or a decimal point (.) followed by at least one digit for the fractional part
function scanner:number()
    while scanner.isdigit(self:peek()) do self:advance() end

    -- Look for a fractional part.
    if (self:peek() == '.') and scanner.isdigit(self:peeknext()) then
        -- Consume the "."
        self:advance()

        while scanner.isdigit(self:peek()) do self:advance() end
    end

    self:addtoken(tokentypes.NUMBER, tonumber(self.source:sub(self.start, self.current - 1)))
end

-- Consume characters until we hit the " that ends the string
function scanner:string()
    while (self:peek() ~= '"') and (not self:isatend()) do
        if self:peek() == '\n' then self.line = self.line + 1 end
        self:advance()
    end

    if self:isatend() then
        utils.error(self.line, "Unterminated string.")
        return
    end

    -- The closing ".
    self:advance()

    -- Trim the surrounding quotes.
    local value = self.source:sub(self.start + 1, self.current - 2)
    self:addtoken(tokentypes.STRING, value)
end

-- Consume the next character only if is that we're looking for
function scanner:match(expected)
    if self:isatend() then return false end
    if self.source:sub(self.current, self.current) ~= expected then return false end

    self.current = self.current + 1
    return true
end

-- Get one character lookahead
function scanner:peek()
    if self:isatend() then return '\0' end
    return self.source:sub(self.current, self.current)
end

-- Get one character lookahead at two characters
function scanner:peeknext()
    if (self.current + 1) > self.source:len() then return '\0' end
    return self.source:sub(self.current + 1, self.current + 1)
end

function scanner.isdigit(char)
    return char:byte() >= ('0'):byte() and char:byte() <= ('9'):byte()
end

function scanner.isalpha(char)
    return (char:byte() >= ('a'):byte() and char:byte() <= ('z'):byte()) or
            (char:byte() >= ('A'):byte() and char:byte() <= ('Z'):byte()) or
            char == '_'
end

function scanner.isalphanumeric(char)
    return scanner.isalpha(char) or scanner.isdigit(char)
end

-- Check if we are at the end of source
function scanner:isatend()
    return self.current > self.source:len()
end

-- Get the current character and advance one position
function scanner:advance()
    local char = self.source:sub(self.current, self.current)
    self.current = self.current + 1
    return char
end

-- Add a token to the list
function scanner:addtoken(type, literal)
    local text = self.source:sub(self.start, self.current - 1)
    table.insert(self.tokens, token.new(type, text, literal, self.line))
end

return scanner
