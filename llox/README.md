# llox

A lox tree-walk interpreter made in Lua 5.4

For running use the [llox](./llox) bash script:

```
$ ./llox <arguments>
```

Also, you can run [llox.lua](./llox.lua) file:

```
$ lua ./llox.lua <arguments>
```