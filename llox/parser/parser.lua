local tokentypes = require "scanner.tokentypes"
local expr = require "grammar.expr"
local stmt = require "grammar.stmt"
local utils = require "utils"
local astprinter = require "astprinter"

local parser = {}

-- Creates a new parser
function parser.new(tokens)
    return setmetatable({
        tokens = tokens,
        current = 1,
    }, {
        __index = utils.__index(parser)
    })
end

-- Parsing the grammar of a token list
-- program -> declaration* EOF;
function parser:parse()
    local statements = {}
    while not self:isatend() do
        local statement = self:declaration()
        table.insert(statements, statement)
    end

    return statements
end

-- expression -> assignment;
function parser:expression()
    return self:assignment()
end

-- declaration -> varDec | statement;
function parser:declaration()
    local ret, ex;
    if self:match(tokentypes.VAR) then
        ret, ex = self:vardeclaration()
    else
        ret, ex = self:statement()
    end

    if ex == "Parser Error" then
        self:synchronize()
        return nil
    else
        return ret, ex
    end
end

-- statement -> exprStmt | printStmt | block;
function parser:statement()
    if self:match(tokentypes.PRINT) then return self:printstatement() end
    if self:match(tokentypes.LEFT_BRACE) then
        local statements, ex = self:block()
        if ex ~= nil then return nil, ex end
        return stmt.block.new(statements)
    end

    return self:expressionstatement()
end

-- printStmt -> "print" expression ";";
function parser:printstatement()
    local value = self:expression()
    local ok, ex = self:consume(tokentypes.SEMICOLON, "Expected ';' after value.");
    if not ok then return nil, ex end
    return stmt.print.new(value)
end

-- varDev -> "var" IDENTIFIER ("=" expression)? ";";
function parser:vardeclaration()
    local name, ex = self:consume(tokentypes.IDENTIFIER, "Expect variable name.")
    if not name then return nil, ex end

    local initializer
    if self:match(tokentypes.EQUAL) then
        local ex1
        initializer, ex1 = self:expression()
        if ex1 ~= nil then return nil, ex1 end
    end

    local ok, ex1 = self:consume(tokentypes.SEMICOLON, "Expect ';' after variable declaration.")
    if not ok then return nil, ex1 end

    return stmt.var.new(name, initializer)
end

-- exprStmt -> expression ";";
function parser:expressionstatement()
    local value, ex = self:expression()
    if ex ~= nil then return nil, ex end
    local ok, msg = self:consume(tokentypes.SEMICOLON, "Expected ';' after value.");
    if not ok then return nil, msg end
    return stmt.expression.new(value)
end

-- block -> "{" declaration* "}";
function parser:block()
    local statements = {}

    while not self:check(tokentypes.RIGHT_BRACE) and not self:isatend() do
        local statement, ex = self:declaration()
        if ex ~= nil then return nil, ex end
        table.insert(statements, statement)
    end

    local ok, ex = self:consume(tokentypes.RIGHT_BRACE, "Expected '}' after block.")
    if not ok then return nil, ex end
    return statements
end

-- assignment -> IDENTIFIER "=" assignment | equality;
function parser:assignment()
    local expression, ex = self:equality()
    if ex ~= nil then return nil, ex end

    if self:match(tokentypes.EQUAL) then
        local equals = self:previous()
        local value, ex1 = self:assignment()
        if ex1 ~= nil then return nil, ex1 end

        if expression ~= nil and expression.type == "variable" then
            local name = expression.name
            return expr.assign.new(name, value)
        end

        parser.error(equals, "Invalid assignment target.")
    end

    return expression
end

-- equality -> comparison (("!=" | "==") comparison)*;
function parser:equality()
    local expression, ex = self:comparison()
    if ex ~= nil then return nil, ex end

    while self:match(tokentypes.BANG_EQUAL, tokentypes.EQUAL_EQUAL) do
        local operator, ex1 = self:previous()
        if ex1 ~= nil then return nil, ex1 end
        local right = self:comparison()
        expression = expr.binary.new(expression, operator, right)
    end

    return expression
end

-- comparison -> term ((">" | ">=" | "<" | "<=") term)*;
function parser:comparison()
    local expression, ex = self:term()
    if ex ~= nil then return nil, ex end

    while self:match(tokentypes.GREATER, tokentypes.GREATER_EQUAL, tokentypes.LESS, tokentypes.LESS_EQUAL) do
        local operator = self:previous()
        local right, ex1 = self:term()
        if ex1 ~= nil then return nil, ex1 end
        expression = expr.binary.new(expression, operator, right)
    end

    return expression
end

-- term -> factor (("-" | "+") factor)*;
function parser:term()
    local expression, ex = self:factor()
    if ex ~= nil then return nil, ex end

    while self:match(tokentypes.MINUS, tokentypes.PLUS) do
        local operator = self:previous()
        local right, ex1 = self:factor()
        if ex1 ~= nil then return nil, ex1 end
        expression = expr.binary.new(expression, operator, right)
    end

    return expression
end

-- factor -> unary (( "/" | "*" ) unary)*;
function parser:factor()
    local expression, ex = self:unary()
    if ex ~= nil then return nil, ex end

    while self:match(tokentypes.SLASH, tokentypes.STAR) do
        local operator = self:previous()
        local right, ex1 = self:unary()
        if ex1 ~= nil then return nil, ex end
        expression = expr.binary.new(expression, operator, right)
    end

    return expression
end

-- unary -> ("!" | "-") unary |
--          primary;
function parser:unary()
    if self:match(tokentypes.BANG, tokentypes.MINUS) then
        local operator = self:previous()
        local right, ex = self:unary()
        if ex ~= nil then return nil, ex end
        return expr.unary.new(operator, right)
    end

    return self:primary()
end

-- primary -> NUMBER | STRING |
--            "true" | "false" | "nil" |
--            "(" expression ")";
function parser:primary()
    if self:match(tokentypes.FALSE) then return expr.literal.new(false) end
    if self:match(tokentypes.TRUE) then return expr.literal.new(true) end
    if self:match(tokentypes.NIL) then return expr.literal.new(nil) end

    if self:match(tokentypes.IDENTIFIER) then
        return expr.variable.new(self:previous())
    end

    if self:match(tokentypes.NUMBER, tokentypes.STRING) then
        return expr.literal.new(self:previous().literal)
    end

    if self:match(tokentypes.LEFT_PAREN) then
        local expression = self:expression()
        local ok, msg = self:consume(tokentypes.RIGHT_PAREN, "Expect ')' after expression.")
        if not ok then return nil, msg end
        return expr.grouping.new(expression)
    end

    return parser.error(self:peek(), "Expect expression.")
end

-- Checks if the current token has any of the given types
function parser:match(...)
    for _, type in ipairs({...}) do
        if self:check(type) then
            self:advance()
            return true
        end
    end

    return false
end

-- Like match only consume the token if exists, but raised an error if there isn't
function parser:consume(type, message)
    if self:check(type) then return self:advance() end
    return parser.error(self:peek(), message)
end

-- Returns true if the current tokken is of the given type
function parser:check(type)
    if self:isatend() then return false end
    return self:peek().type == type
end

-- Consume the current token and returns it
function parser:advance()
    if not self:isatend() then self.current = self.current + 1 end
    return self:previous()
end

-- Checks if we've run out of tokens to parse
function parser:isatend()
    return self:peek().type == tokentypes.EOF
end

-- Returns the current token we have yet to consume
function parser:peek()
    return self.tokens[self.current]
end

-- Returns most recently consumed token
function parser:previous()
    return self.tokens[self.current - 1]
end

-- Report and error consuming the tokens and raised an error
function parser.error(token, message)
    utils.errortoken(token, message)
    if utils.debug then
        print(debug.traceback())
    end
    return nil, "Parser error"
end

-- Discards tokens until it thinks it has found a statement boundary
function parser:synchronize()
    self:advance()

    while not self:isatend() do
        if self:previous().type == tokentypes.SEMICOLON then return end

        if self:peek().type == tokentypes.CLASS or
                self:peek().type == tokentypes.FUN or
                self:peek().type == tokentypes.VAR or
                self:peek().type == tokentypes.FOR or
                self:peek().type == tokentypes.IF or
                self:peek().type == tokentypes.WHILE or
                self:peek().type == tokentypes.PRINT or
                self:peek().type == tokentypes.RETURN then
            return
        end

        self:advance()
    end
end

return parser
