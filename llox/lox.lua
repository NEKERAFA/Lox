local scanner = require "scanner.scanner"
local parser = require "parser.parser"
local interpreter = require "interpreter.interpreter"
local utils = require "utils"
local astprinter = require "astprinter"

local lox = {}
local itp = interpreter.new()

-- Run the source code as string
local function run(source)
    local sc = scanner.new(source)
    local tokens = sc:scantokens()

    if utils.debug then
        print("-- scanner")
        for _, token in ipairs(tokens) do
            print(token)
        end
    end

    local ps = parser.new(tokens)
    local statements, msg = ps:parse()

    -- Stop if there is a syntax error
    if utils.haderror then print(msg); return end

    if utils.debug then
        print("-- parser")
        astprinter.new():print(statements)
        print("-- interpreter")
    end

    itp:interpret(statements)
end

-- Loads a entire file and run it
function lox.runfile(path)
    local fd = assert(io.open(path, "r"))
    local buffer = fd:read("a")
    fd:close()
    run(buffer)

    -- Indicate an error in the exit code.
    if utils.haderror then os.exit(65) end
    if utils.hadruntimeerror then os.exit(70) end
end

-- Run interactively from stdin
function lox.runprompt()
    while true do
        io.write("> ")
        local line = io.read()
        if line == nil or string.len(line) == 0 then break end
        run(line)
        utils.haderror = false
    end
end

return lox
