local environment = require "interpreter.environment"
local tokentypes = require "scanner.tokentypes"
local utils = require "utils"

local interpreter = {}

function interpreter.new()
    return setmetatable({
        environment = environment.new()
    }, {
        __index = utils.__index(interpreter)
    })
end

-- Interpret the expression
function interpreter:interpret(statements)
    local ex
    for _, statement in ipairs(statements) do
        local _, exception = self:execute(statement)
        if exception ~= nil then
            ex = exception
            break
        end
    end

    if ex ~= nil then utils.runtimeerror(ex) end
end

-- Appends the expression to evaluate like a interpreter's visitor
function interpreter:evaluate(expression)
    if utils.debug then print("evaluate: ", tostring(expression)) end
    return expression:accept(self)
end

-- The statement analogue to evaluate() method for expressions
function interpreter:execute(statement)
    return statement:accept(self)
end

-- Execute the block using new scope
function interpreter:executeblock(statements, environment)
    local previous = self.environment
    self.environment = environment

    local exception = nil
    for _, statement in ipairs(statements) do
        local _, ex = self:execute(statement)
        if ex ~= nil then
            exception = ex
            break
        end
    end

    self.environment = previous
    return nil, exception
end

-- Evaluates a block and visit it
function interpreter:visitblockstmt(statement)
    return self:executeblock(statement.statements, environment.new(self.environment))
end

-- Evaluates the inner expression of the statement
function interpreter:visitexpressionstmt(statement)
    local _, ex = self:evaluate(statement.innerexpr)
    if ex ~= nil then return nil, ex end
end

-- Evaluates the inner expression and print the result
function interpreter:visitprintstmt(statement)
    local value, ex = self:evaluate(statement.innerexpr)
    if ex ~= nil then return nil, ex end
    print(tostring(value))
end

-- Binds a variable, evaluating it initializer
function interpreter:visitvarstmt(statement)
    local value, ex
    if statement.initializer ~= nil then
        value, ex = self:evaluate(statement.initializer)
        if ex ~= nil then return nil, ex end
    end

    self.environment:define(statement.name.lexeme, value)
    return nil
end

function interpreter:visitassignexpr(expression)
    local value, ex = self:evaluate(expression.value)
    if ex ~= nil then return nil, ex end
    local ok, ex1 = self.environment:assign(expression.name, value)
    if not ok then return nil, ex1 end
    return value;
end

-- Evaluates both operands and then apply the binary operator
function interpreter:visitbinaryexpr(expression)
    local left, ex1 = self:evaluate(expression.left)
    if ex1 ~= nil then return nil, ex1 end
    local right, ex2 = self:evaluate(expression.right)
    if ex2 ~= nil then return nil, ex2 end

    if expression.operator.type == tokentypes.GREATER then
        local ok, ex = interpreter.checknumbers(expression.operator, left, right)
        if not ok then return nil, ex end
        return tonumber(left) > tonumber(right)
    elseif expression.operator.type == tokentypes.GREATER_EQUAL then
        local ok, ex = interpreter.checknumbers(expression.operator, left, right)
        if not ok then return nil, ex end
        return tonumber(left) >= tonumber(right)
    elseif expression.operator.type == tokentypes.LESS then
        local ok, ex = interpreter.checknumbers(expression.operator, left, right)
        if not ok then return nil, ex end
        return tonumber(left) < tonumber(right)
    elseif expression.operator.type == tokentypes.LESS_EQUAL then
        local ok, ex = interpreter.checknumbers(expression.operator, left, right)
        if not ok then return nil, ex end
        return tonumber(left) <= tonumber(right)
    elseif expression.operator.type == tokentypes.MINUS then
        local ok, ex = interpreter.checknumbers(expression.operator, left, right)
        if not ok then return nil, ex end
        return tonumber(left) - tonumber(right)
    elseif expression.operator.type == tokentypes.PLUS then
        if type(left) == "number" and type(right) == "number" then
            return left + right
        end

        if type(left) == "string" and type(right) == "string" then
            return left .. right
        end

        return utils.getruntimeerror(expression.operator, "Operands must two numbers or two strings.")
    elseif expression.operator.type == tokentypes.SLASH then
        local ok, ex = interpreter.checknumbers(expression.operator, left, right)
        if not ok then return nil, ex end
        return tonumber(left) / tonumber(right)
    elseif expression.operator.type == tokentypes.STAR then
        local ok, ex = interpreter.checknumbers(expression.operator, left, right)
        if not ok then return nil, ex end
        return tonumber(left) * tonumber(right)
    elseif expression.operator.type == tokentypes.EQUAL_EQUAL then
        return interpreter.isequal(left, right)
    elseif expression.operator.type == tokentypes.BANG_EQUAL then
        return not interpreter.isequal(left, right)
    end

    -- unreachable
    return nil
end

-- Converts literal token into a literal syntax tree node
function interpreter:visitliteralexpr(expression)
    return expression.value
end

-- Evaluates the operand expression and then apply the unary operator
function interpreter:visitunaryexpr(expression)
    local right, ex1 = self:evaluate(expression.right)
    if ex1 ~= nil then return nil, ex1 end

    if expression.operator.type == tokentypes.BANG then
        return not interpreter.istruthy(right)
    elseif expression.operator.type == tokentypes.MINUS then
        local ok, ex = interpreter.checknumber(expression.operator, right)
        if not ok then return nil, ex end
        return -tonumber(right)
    end

    -- unreachable
    return nil
end

-- Forwards to the environment to check if the variable is defined
function interpreter:visitvariableexpr(expression)
    return self.environment:get(expression.name)
end

-- Evaluates the inner expression of a group
function interpreter:visitgroupingexpr(expression)
    return self:evaluate(expression.expression)
end

-- Checks if the operand is a number
function interpreter.checknumber(operator, operand)
    if type(operand) == "number" then return true end
    return utils.getruntimeerror(operator, "Operand must be a number.")
end

-- Checks if both operands are numbers
function interpreter.checknumbers(operator, left, right)
    if type(left) == "number" and type(right) == "number" then return true end
    return utils.getruntimeerror(operator, "Operands must be numbers.")
end

-- Converts a value into a boolean value
-- In Lua, false and nil are falsey, and everything else is truthy
function interpreter.istruthy(value)
    return value == true
end

-- Compares if two elements are equals
function interpreter.isequal(left, right)
    return left == right
end

return interpreter