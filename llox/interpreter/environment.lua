local utils = require "utils"

local environment = {}

-- Creates a new environment
function environment.new(enclosing)
    return setmetatable({
        enclosing = enclosing,
        values = {}
    }, {
        __index = utils.__index(environment)
    })
end

function environment:define(name, value)
    self.values[name] = { value }
end

function environment:get(token)
    if self.values[token.lexeme] ~= nil then
        return self.values[token.lexeme][1]
    end

    if self.enclosing ~= nil then return self.enclosing:get(token) end

    return utils.getruntimeerror(token, ("Undefined variable %q."):format(tostring(token.lexeme)))
end


function environment:assign(token, value)
    if self.values[token.lexeme] ~= nil then
        self.values[token.lexeme][1] = value
        return true
    end

    if self.enclosing ~= nil then
        return self.enclosing:assign(token, value)
    end

    return utils.getruntimeerror(token, ("Undefined variable %q."):format(tostring(token.lexeme)))
end

return environment