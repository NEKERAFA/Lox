local tokentypes = require "scanner.tokentypes"

local utils = {
    debug = false,
    haderror = false,
    hadruntimeerror = true
}

function utils.__index(metatable)
    return function(_, key)
        if key ~= "new" then
            return metatable[key]
        end
    end
end

function utils.__tostring(tbl)
    local buff = ''
    local first = true
    for k, v in pairs(tbl) do
        buff = ('%s%s%s = '):format(buff, first and '' or ', ', k)
        if type(v) == "table" then
            buff = ('%s%s'):format(buff, utils.__tostring(v))
        else
            buff = ('%s%q'):format(buff, tostring(v))
        end
        first = false
    end
    return ('{%s}'):format(buff)
end

-- Report an error
local function report(line, where, message)
    print(("[line %i] Error%s: %s\n"):format(line, where, message))
    utils.haderror = true
end

-- Throws an error
function utils.error(line, message)
    report(line, "", message)
end

function utils.errortoken(token, message)
    if (token.type == tokentypes.EOF) then
        report(token.line, " at end", message)
    else
        report(token.line, (" at %q"):format(token.lexeme), message)
    end
end

function utils.getruntimeerror(token, message)
    return nil, { token = token, message = message }
end

function utils.runtimeerror(exception)
    print(("[line %s] %s"):format(exception.token.line, exception.message))
    utils.hadruntimeerror = true
end

return utils
